jQuery.sap.require("sap.ui.demo.tdg.util.Formatter");

sap.ui.core.mvc.Controller.extend("sap.ui.demo.tdg.view.Detail",{
	onInit:function(){
		var oView = this.getView();
		sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent){
			//update the binding context for detail navigation
			if (oEvent.getParameter("name")==="product"){
				var sProductPath = "/" + oEvent.getParameter("arguments").product;
				oView.bindElement(sProductPath);
				
				//check the product was actually found
				oView.getElementBinding().attachEventOnce("dataRecieved",
						jQuery.proxy(function(){
							var oData = oView.getModel().getData(sProductPath);
							if (!oData){
								sap.ui.core.UIComponent.getRouterFor(this).myNavToWithoutHash({
									currentView:oView,
									targetViewName:"sap.ui.demo.tdg.view.NotFound",
									targetViewType:"XML"
								});
							}
						},this));
				//master is here?
				var oIconTabBar = oView.byId("idIconTabBar");
				oIconTabBar.getItems().forEach(function(oItem){
					oItem.bindElement(sap.ui.demo.tdg.util.Formatter.upperCaseFirstChar(oItem.getKey()));					
				});
				//which tab?
				var sTabKey = oEvent.getParameter("arguments").tab || "supplier";
				if (oIconTabBar.getSelectedKey() !== sTabKey ){
					oIconTabBar.setSelectedKey(sTabKey);					
				}
			}
			
		}, this);
	},
	onNavBack:function(){
		sap.ui.core.UIComponent.getRouterFor(this).myNavBack("main");
	},
	
	onDetailSelect:function(oEvent){
		sap.ui.core.UIComponent.getRouterFor(this).navTo("product",{
				product: oEvent.getSource().getBindingContext().getPath().slice(1),
				tab:oEvent.getParameter("selectedKey")
		},true);
	}
	
});