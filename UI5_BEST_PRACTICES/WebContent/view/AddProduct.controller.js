sap.ui.core.mvc.Controller.extend("sap.ui.demo.tdg.view.AddProduct",{
	oAlertDialog:null,
	oBusyDialog:null,
	onInit:function(){
		this.getView().setModel(new sap.ui.model.json.JSONModel(),"newProduct");
		this.initializeNewProductData();
	},
	initializeNewProductData:function(){
		this.getView().getModel("newProduct").setData({
			Detail:{ Discontinuedflag : false }
		});
	},
	showErrorAlert:function(sMessage){
		jQuery.sap.require("sap.m.MessageBox");
		sap.m.MessageBox.alert(sMessage);
	},
	dateFromString:function(sDate){
		var oDate=new Date(sDate);
		return oDate=="Invalid Date"?new Date(sDate.split("/").reverse()):oDate;
	},
	saveProduct:function(nID){
		var mNewProduct=this.getView().getModel("newProduct").getData().Detail;
		var mPayLoad={
				ID:nID,
				Name:mNewProduct.Name,
				Description:mNewProduct.Description,
				ReleaseDate:this.dateFromString(mNewProduct.ReleaseDate),
				Price:mNewProduct.Price.toString(),
				Rating:mNewProduct.Rating
		};
		if (mNewProduct.DiscontinuedDate){
			mPayLoad.DiscontinuedDate=this.dateFromString(mNewProduct.DiscontinuedDate);
		}
		["Supplier","Category"].forEach(function(sRelation){
			var oSelect = this.getView().byId("idSelect"+sRelation);
			var sPath=oSelect.getSelectedItem().getBindingContext().getPath();
			mPayLoad[sRelation]={
					__metadata:{
						uri:sPath
					}
			};
		},this);
		
		var oModel=this.getView().getModel();
		oModel.create("/Products",mPayLoad,{
			success:jQuery.proxy(function(mResponse){
				this.initializeNewProductData();
				this.oBusyDialog.close();
				jQuery.sap.require("sap.m.MessageToast");
				sap.m.MessageToast.show("Product '" +mPayLoad.Name +"' added");				
				sap.ui.core.UIComponent.getRouterFor(this).navTo("product",{
					from:"master",
					product:"Products("+mResponse.ID+")",
					tab:"supplier"
				},false);
			},this),
			error:jQuery.proxy(function(){
				this.oBusyDialog.close();
				this.showErrorAlert("Problem creating new Product");
			},this)
		},this);
	
	},
	onSave:function(){
		if(!this.getView().getModel("newProduct").getProperty("/Detail/Name")){
			if(!this.oAlertDialog){
				this.oAlertDialog=sap.ui.xmlFragment("sap.ui.tdg.view.NameRequiredDialog",this);
				this.getView().addDependent(this.oAlertDialog);				
			}
			this.oAlertDialog.open();
		}else{
			if(!this.oBusyDialog){
				this.oBusyDialog = new sap.m.BusyDialog();
			}
			this.oBusyDialog.open();
			this.getView().getModel().read("/Products",{
				urlParameters:{
					"$top":1,
					"$orderby":"ID desc",
					"$select":"ID"
					},
				async:false,
				success:jQuery.proxy(function(oData){
					this.saveProduct(oData.results[0].ID+1);
				},this),
				error:jQuery.proxy(function(){
					this.oBusyDialog.close();	
					this.showAlert("Cannot determine next ID for new product");
				},this),
				
			});

		}
		
	},
	onCancel:function(){
		sap.ui.core.UIComponent.getRouterFor(this).backWithoutHash(this.getView());
	},
	onDialogClose:function(oEvent){
		oEvent.getSource().getParent().close();
	}
	

});